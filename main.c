#include <stdio.h>
#include <limits.h>
int main(void)
{
    float i = 3.5;
    int Buchstabe = 280;
    float flaeche = 1.23e+5;
    double Max_X_Achse = 40000;
    unsigned short Max_Y_Achse = 50000;
    int k = 7;
    double umfang = 12345.6470589;
    double lzaehler = 3e9;
    double llzaehler = 3e9;
    printf("i = %f\n", i);
    printf("Buchstabe = %d\n", Buchstabe);
    printf("flaeche = %f\n", flaeche);
    printf("Max_X_Achse = %lf\n", Max_X_Achse);
    printf("Max_Y_Achse = %hu\n", Max_Y_Achse);
    printf("5k = %d\n", k);
    printf("umfang = %f\n", umfang);
    printf("lzaehler = %lf\n", lzaehler);
    printf("long min = %ld, long max = %ld\n",LONG_MIN,LONG_MAX);
    printf("llzaehler = %lf\n", llzaehler);
    printf("sizeof(long): %d Byte\n", sizeof(long) );

    return 0;

}